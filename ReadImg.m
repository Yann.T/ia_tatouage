function out = ReadImg(file)
    out = imread(file);
    imshow(out);
end